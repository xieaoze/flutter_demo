import 'package:flutter/material.dart';
import 'package:hello_janson/TabBarWidget.dart';
import 'package:hello_janson/TabBarPageItem.dart';

class TabBarPageWidget extends StatefulWidget {
  @override
  _TabBarPageWidgetState createState() => _TabBarPageWidgetState();
}

class _TabBarPageWidgetState extends State<TabBarPageWidget> {
  final PageController topPageControl = new PageController();
  final List<String> tab = ['111', '222', '333', '444'];

  _renderTab() {
    List<Widget> list = new List();
    for (int i = 0; i < tab.length; i++) {
      list.add(new FlatButton(
          onPressed: () {
            topPageControl.jumpTo(MediaQuery.of(context).size.width * i);
          },
          child: new Text(
            tab[i],
            maxLines: 1,
          )));
    }
    return list;
  }

  _renderPage() {
    return [
      new TabBarPageItem(),
      new TabBarPageItem(),
      new TabBarPageItem(),
      new TabBarPageItem(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new TabBarWidget(
      type: TabBarWidget.TOP_TAB,
      tabItems: _renderTab(),
      tabViews: _renderPage(),
      topPageControl: topPageControl,
      backgroundColor: Colors.lightBlue,
      indicatorColor: Colors.white,
      title: new Text('Test'),
    );
  }
}
