import 'package:flutter/material.dart';
import 'package:hello_janson/TabBarBottomPageWidget.dart';
import 'package:hello_janson/TabBarPageWidget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // final wordPair=new WordPair.random();
    return new MaterialApp(
      title: "Flutter Demo",
      theme: new ThemeData(primarySwatch: Colors.blue),
      home: new MainPage(),
    );
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Title"),
      ),
      body: new Column(
        children: <Widget>[
          new Expanded(
            child: new Center(
              child: new FlatButton(
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new TabBarPageWidget()));
                },
                child: new Text("Top Tab"),
              ),
            ),
          ),
          new Expanded(
            child: new Center(
              child: new FlatButton(
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new TabBarBottomPageWidget()));
                },
                child: new Text("Bottom Tab"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
